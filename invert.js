function invert(testObject) {

    var invertedObj = Object.fromEntries(
        Object.entries(testObject).map(([key, value]) => [value, key])
    );

    return invertedObj;

}

module.exports = invert;