const testObject = require('../objectData');
const defaults = require('../defaults');

defaults(testObject, (testObject, obj) => {
    //copy one object to other using Object.assign(target, source);
    Object.assign(testObject, obj);
    return testObject;
})